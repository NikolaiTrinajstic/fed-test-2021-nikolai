import React from 'react';

import './CircleComponent.css';

const CircleComponent = ({ copy, icon }) => (
    <div className="circle-component">
        <div className="circleStyle">
            <p className="circle-icon-container">
                {icon}
            </p>
        </div>
        <div className="circle-text-container">
            <p>{copy}</p>
        </div>
    </div>
);

export default CircleComponent;