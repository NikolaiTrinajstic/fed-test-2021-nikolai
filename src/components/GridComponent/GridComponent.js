import React from 'react';
import { Grid } from '@material-ui/core';
import PanToolOutlinedIcon from '@material-ui/icons/PanToolOutlined';

import CheckedTable from '../CheckedTable/CheckedTable.js';
import TableForm from '../TableForm/TableForm.js';
import CircleComponent from '../CircleComponent/CircleComponent.js';

import './GridComponent.css';

export default class GridComponent extends React.Component {

  render() {

    return (
      <div className="grid">
        <h2 className="essentials-header">Essentials</h2>
        <Grid container spacing={7}>
          <Grid item xs={12} md={6}>
            <p className="essentials-subheader">Great value health cover to fit your lifestyle from less than $4.40 a week^</p>
            <p className="essentials-copy">A healthy body can do amazing things! Look after yours with Essentials Saver. You're instantly covered for Urgent Ambulance and General Dental, and you get to choose two more services you will use, so you don't pay for what you won't.</p>
            <CircleComponent
              copy="Four essential services to fit your lifestyle"
              icon="4"
            />
            <CircleComponent
              copy="Choose the 2 services that you will use and get 2 more"
              icon={
                <PanToolOutlinedIcon style={
                  {
                    fontSize: 40,
                    position: 'absolute',
                    left: -10,
                    top: -15,
                  }
                } />
              }
            />
          </Grid>
          <Grid item xs={12} md={5}>
            <h3 className="essentials-header">Customise your cover</h3>
            <div className="table-component">
              <CheckedTable />
              <TableForm />
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}