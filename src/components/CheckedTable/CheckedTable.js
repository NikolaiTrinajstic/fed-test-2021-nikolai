import React from 'react';
import Done from '@material-ui/icons/Done';

import { COLORS } from '../../constants/constants.js';
import '../TableForm/TableForm.css';

export default class CheckedTable extends React.Component {

  render() {

    return (
      <div className="table-container">
        <p className="table-header">Your cover includes:</p>
        <div className="row checked-row">
          <p>Urgent Ambulance</p>
          <Done style={
            {
              color: COLORS.lightGrey,
              position: 'absolute',
              right: 20,
              top: 17,
            }
          } />
        </div>
        <div className="row checked-row">
          <p>General Dental</p>
          <Done style={
            {
              color: COLORS.lightGrey,
              position: 'absolute',
              right: 20,
              top: 17,
            }
          } />
        </div>
      </div>
    );
  }
}