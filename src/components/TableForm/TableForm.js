import React from 'react';
import { FormGroup, Checkbox } from '@material-ui/core';
import { Done, ChevronRight } from '@material-ui/icons';

import { COLORS } from '../../constants/constants.js';

import './TableForm.css';

export default function TableForm() {
  const [options, setOptions] = React.useState({
    'General Dental': false,
    'Optical': false,
    'Physio': false,
    'Podiatry': false,
    'Pharmacy': false,
    'Chiro/Osteo': false,
    'Remedial Massage': false,
  });

  const [isFirstEntry, setEntry] = React.useState(true)

  const handleChange = (event) => {
    setOptions({ ...options, [event.target.name]: event.target.checked });
    setEntry(false);
  };

  const handleError = (values) => {
    if (values.length < 2) {
      return 'Please select 2 or more services to continue'
    } else if (values.length >= 6) {
      return 'Please select 5 or fewer services to continue'
    }
  };

  React.useEffect(() => {
    const parsedOptions = JSON.parse(localStorage.getItem("options"));
    if (parsedOptions) {
      setOptions(parsedOptions);
    }
  }, [])

  React.useEffect(() => {
    localStorage.setItem("options", JSON.stringify(options));
  }, [options])

  const values = Object.values(options).filter(value => value === true)
  const isDisabled = values.length < 2 || values.length >= 6
  console.log(isFirstEntry)

  const TableRow = ({ checked, name, handleChange }) => {
    return (
      <div className={!checked ? 'row unchecked-row' : 'row checked-row'}>
        <p>{name}</p>
        <div className="checkbox-container">
          <Checkbox
            className="table-checkbox"
            checked={checked}
            onChange={handleChange}
            name={name}
            checkedIcon={
              <span className="unchecked-icon">
                <Done style={
                  {
                    color: COLORS.lightGreen,
                  }
                } />
              </span>
            }
            icon={<span className="unchecked-icon" />}
          />
        </div>
      </div>
    )
  }

  return (
    <div className="table-container">
      <FormGroup>
        <p className="table-header">Your cover includes:</p>
        {options && Object.keys(options).map((option, i) => {
          return (
            <TableRow
              checked={options[option]}
              handleChange={handleChange}
              name={option}
            />
          )
        })}
        <div className="table-button-container">
          <button className="table-button" disabled={isDisabled}>
            <p>Get a quote <ChevronRight style={
              {
                color: '#FBCD7A',
                verticalAlign: 'middle',
                display: 'inline-block',
              }
            } /></p>
          </button>
        </div>
        {isDisabled && !isFirstEntry &&
          <div className="table-error-container">
            <p>{handleError(values)}</p>
          </div>
        }
      </FormGroup>
    </div>
  );
}
