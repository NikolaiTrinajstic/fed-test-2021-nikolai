import React from "react";

import '../styles/Base.css';
import '../styles/Typography.css';
import '../styles/Grid.css';
import GridComponent from './GridComponent/GridComponent';

export default class App extends React.Component {

  render() {
    return (
      <div className="App">
        <GridComponent />
      </div>
    )
  }
}
